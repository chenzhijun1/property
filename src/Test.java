import entity.InnerOSEntity;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * title:
 * description:
 * Company: 易宝天创(dataYP.com)
 * Package: PACKAGE_NAME
 * ClassName: Test
 * Author zhijun.chen
 * CreateTime: 2017/1/16 10:04
 * version 1.0.0
 */
public class Test {

    public static void main(String[] args) {
        InnerOSEntity innerOSEntity = new InnerOSEntity();
        Method[] methods = innerOSEntity.getClass().getMethods();
        System.out.print("method name : ");
        for (Method method : methods) {
            System.out.print(method.getName() + ", ");
        }
        System.out.println();
        System.out.println("field name : ");
        Field[] fields = innerOSEntity.getClass().getFields();
        for (Field field : fields) {
            field.setAccessible(true);
            System.out.print(field.getName() + ", ");
        }


        System.out.println("================================");

        System.out.print("declared method name : ");
        methods = innerOSEntity.getClass().getDeclaredMethods();
        for (Method method : methods) {
            System.out.print(method.getName() + ",");
        }
        System.out.println();
        System.out.print("declared field name:");
        fields = innerOSEntity.getClass().getDeclaredFields();
        for (Field field : fields) {
//            field.setAccessible(true);
            System.out.print(field.getName() + ", ");
        }

        System.out.println();
        System.out.println("===============================");
        System.out.println(innerOSEntity.getClass().getName());
        System.out.println(innerOSEntity.getClass().getSimpleName());
        System.out.println(innerOSEntity.getClass().getCanonicalName());
        System.out.println(innerOSEntity.getClass().getTypeName());
    }

}
