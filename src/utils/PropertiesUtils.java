package utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * title:
 * description:
 * Company: 易宝天创(dataYP.com)
 * Package: utils
 * ClassName: PropertiesUtils
 * Author zhijun.chen
 * CreateTime: 2017/1/16 09:22
 * version 1.0.0
 */
public class PropertiesUtils {

    private static Properties prop = new Properties();
    private static InputStream inStream;

    static {
        try {
            inStream = new FileInputStream("/Users/chenzhijun/workmine/property/src/setting.properties");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try{
            prop.load(new InputStreamReader(inStream,"UTF-8"));
        }catch(Exception e){
            prop = null;
        }
    }

    private PropertiesUtils(){
    }

    public static String getValueByKey(String key){
        if(prop == null){
            System.out.println("prop is null");
        }
        return prop.getProperty(key);
    }
}
