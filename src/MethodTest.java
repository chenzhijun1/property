import entity.InnerOSEntity;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * title:
 * description:
 * Company: 易宝天创(dataYP.com)
 * Package: PACKAGE_NAME
 * ClassName: MethodTest
 * Author zhijun.chen
 * CreateTime: 2017/1/16 10:31
 * version 1.0.0
 */
public class MethodTest {
    public static void main(String[] args) throws Exception {
        testMethod();
    }

    public static void testMethod() throws Exception {
        InnerOSEntity innerOSEntity = new InnerOSEntity();
        innerOSEntity.setName("this is my name");

//        Class innerClass = InnerOSEntity.class;
//        Method[] methods = innerClass.getDeclaredMethods();
//        System.out.print("method name : ");
//        for (Method method : methods) {
////            method.invoke()
//        }

        Method method = innerOSEntity.getClass().getDeclaredMethod("getName");

        Object a = method.invoke(innerOSEntity);
        System.out.println(a);

    }

}
