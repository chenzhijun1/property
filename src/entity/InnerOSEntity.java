package entity;

/**
 * title:
 * description:
 * Company: 易宝天创(dataYP.com)
 * Package: entity
 * ClassName: InnerOSEntity
 * Author zhijun.chen
 * CreateTime: 2017/1/16 09:18
 * version 1.0.0
 */
public class InnerOSEntity {
    private Integer id;

    private String name;

    private String createTime;

    private String modifyTime;

    private String inString;

    public String getInString() {
        return inString;
    }

    public void setInString(String inString) {
        this.inString = inString;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(String modifyTime) {
        this.modifyTime = modifyTime;
    }

    @Override
    public String toString() {
        return "InnerOSEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", createTime='" + createTime + '\'' +
                ", modifyTime='" + modifyTime + '\'' +
                ", inString='" + inString + '\'' +
                '}';
    }
}
