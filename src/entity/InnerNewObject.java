package entity;

import java.util.Date;

/**
 * title:
 * description:
 * Company: 易宝天创(dataYP.com)
 * Package: entity
 * ClassName: InnerNewObject
 * Author zhijun.chen
 * CreateTime: 2017/1/16 16:59
 * version 1.0.0
 */
public class InnerNewObject {

    private int aInt;

    private short aShort;

    private byte aByte;

    private char aChar;

    private long aLong;

    private float aFloat;

    private double aDouble;

    private boolean aBoolean;

    private String aString;

    private Date aDate;


    public int getaInt() {
        return aInt;
    }

    public void setaInt(int aInt) {
        this.aInt = aInt;
    }

    public short getaShort() {
        return aShort;
    }

    public void setaShort(short aShort) {
        this.aShort = aShort;
    }

    public byte getaByte() {
        return aByte;
    }

    public void setaByte(byte aByte) {
        this.aByte = aByte;
    }

    public char getaChar() {
        return aChar;
    }

    public void setaChar(char aChar) {
        this.aChar = aChar;
    }

    public long getaLong() {
        return aLong;
    }

    public void setaLong(long aLong) {
        this.aLong = aLong;
    }

    public float getaFloat() {
        return aFloat;
    }

    public void setaFloat(float aFloat) {
        this.aFloat = aFloat;
    }

    public double getaDouble() {
        return aDouble;
    }

    public void setaDouble(double aDouble) {
        this.aDouble = aDouble;
    }

    public boolean isaBoolean() {
        return aBoolean;
    }

    public void setaBoolean(boolean aBoolean) {
        this.aBoolean = aBoolean;
    }

    public String getaString() {
        return aString;
    }

    public void setaString(String aString) {
        this.aString = aString;
    }

    public Date getaDate() {
        return aDate;
    }

    public void setaDate(Date aDate) {
        this.aDate = aDate;
    }

    public InnerNewObject() {
    }

    public InnerNewObject(int aInt, short aShort, byte aByte, char aChar, long aLong, float aFloat, double aDouble, boolean aBoolean, String aString, Date aDate) {
        this.aInt = aInt;
        this.aShort = aShort;
        this.aByte = aByte;
        this.aChar = aChar;
        this.aLong = aLong;
        this.aFloat = aFloat;
        this.aDouble = aDouble;
        this.aBoolean = aBoolean;
        this.aString = aString;
        this.aDate = aDate;
    }

    @Override
    public String toString() {
        return "InnerNewObject{" +
                "aInt=" + aInt +
                ", aShort=" + aShort +
                ", aByte=" + aByte +
                ", aChar=" + aChar +
                ", aLong=" + aLong +
                ", aFloat=" + aFloat +
                ", aDouble=" + aDouble +
                ", aBoolean=" + aBoolean +
                ", aString='" + aString + '\'' +
                ", aDate=" + aDate +
                '}';
    }
}
