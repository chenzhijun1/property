package entity;

/**
 * title:
 * description:
 * Company: 易宝天创(dataYP.com)
 * Package: entity
 * ClassName: MerchantOut
 * Author zhijun.chen
 * CreateTime: 2017/1/16 14:43
 * version 1.0.0
 */
public class MerchantOut {

    private Integer id;

    private String name;

    private String age;

    private String address;

    private String createDate;

    private String price;

    private String score;

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public MerchantOut(Integer id, String name, String age, String address, String createDate, String price) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.address = address;
        this.createDate = createDate;
        this.price = price;
    }

    public MerchantOut() {
    }
}
