package entity;

/**
 * title:
 * description:
 * Company: 易宝天创(dataYP.com)
 * Package: entity
 * ClassName: OuterNewObject
 * Author zhijun.chen
 * CreateTime: 2017/1/16 16:59
 * version 1.0.0
 */
public class OuterNewObject {

    private String bInt;
    private String bShort;
    private String bByte;
    private String bChar;
    private String bLong;
    private String bFloat;
    private String bDouble;
    private String bBoolean;
    private String bString;
    private String bDate;

    public String getbInt() {
        return bInt;
    }

    public void setbInt(String bInt) {
        this.bInt = bInt;
    }

    public String getbShort() {
        return bShort;
    }

    public void setbShort(String bShort) {
        this.bShort = bShort;
    }

    public String getbByte() {
        return bByte;
    }

    public void setbByte(String bByte) {
        this.bByte = bByte;
    }

    public String getbChar() {
        return bChar;
    }

    public void setbChar(String bChar) {
        this.bChar = bChar;
    }

    public String getbLong() {
        return bLong;
    }

    public void setbLong(String bLong) {
        this.bLong = bLong;
    }

    public String getbFloat() {
        return bFloat;
    }

    public void setbFloat(String bFloat) {
        this.bFloat = bFloat;
    }

    public String getbDouble() {
        return bDouble;
    }

    public void setbDouble(String bDouble) {
        this.bDouble = bDouble;
    }

    public String getbBoolean() {
        return bBoolean;
    }

    public void setbBoolean(String bBoolean) {
        this.bBoolean = bBoolean;
    }

    public String getbString() {
        return bString;
    }

    public void setbString(String bString) {
        this.bString = bString;
    }

    public String getbDate() {
        return bDate;
    }

    public void setbDate(String bDate) {
        this.bDate = bDate;
    }

    public OuterNewObject() {
    }

    public OuterNewObject(String bInt, String bShort, String bByte, String bChar, String bLong, String bFloat, String bDouble, String bBoolean, String bString, String bDate) {
        this.bInt = bInt;
        this.bShort = bShort;
        this.bByte = bByte;
        this.bChar = bChar;
        this.bLong = bLong;
        this.bFloat = bFloat;
        this.bDouble = bDouble;
        this.bBoolean = bBoolean;
        this.bString = bString;
        this.bDate = bDate;
    }

    @Override
    public String toString() {
        return "OuterNewObject{" +
                "bInt='" + bInt + '\'' +
                ", bShort='" + bShort + '\'' +
                ", bByte='" + bByte + '\'' +
                ", bChar='" + bChar + '\'' +
                ", bLong='" + bLong + '\'' +
                ", bFloat='" + bFloat + '\'' +
                ", bDouble='" + bDouble + '\'' +
                ", bBoolean='" + bBoolean + '\'' +
                ", bString='" + bString + '\'' +
                ", bDate='" + bDate + '\'' +
                '}';
    }
}
