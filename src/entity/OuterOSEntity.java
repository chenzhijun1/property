package entity;

/**
 * title:
 * description:
 * Company: 易宝天创(dataYP.com)
 * Package: entity
 * ClassName: OuterOSEntity
 * Author zhijun.chen
 * CreateTime: 2017/1/16 09:18
 * version 1.0.0
 */
public class OuterOSEntity {

    private Integer number;

    private String nickName;

    private String createDate;

    private String modifyDate;

    private String outString;

    public String getOutString() {
        return outString;
    }

    public void setOutString(String outString) {
        this.outString = outString;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;
    }

    public OuterOSEntity() {
    }

    public OuterOSEntity(Integer number, String nickName, String createDate, String modifyDate) {
        this.number = number;
        this.nickName = nickName;
        this.createDate = createDate;
        this.modifyDate = modifyDate;
    }

    @Override
    public String toString() {
        return "OuterOSEntity{" +
                "number=" + number +
                ", nickName='" + nickName + '\'' +
                ", createDate='" + createDate + '\'' +
                ", modifyDate='" + modifyDate + '\'' +
                ", outString='" + outString + '\'' +
                '}';
    }
}
