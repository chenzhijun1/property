package entity;

import java.util.Date;

/**
 * title:
 * description:
 * Company: 易宝天创(dataYP.com)
 * Package: entity
 * ClassName: Merchant
 * Author zhijun.chen
 * CreateTime: 2017/1/16 14:41
 * version 1.0.0
 */
public class Merchant {

    private Integer merchantId;

    private String merchantName;

    private String merchantAge;

    private String merchantAddress;

    private Date createDate;

    private Double money;

    private int score;

    public void setMoney(Double money) {
        this.money = money;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public Integer getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(Integer merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMerchantAge() {
        return merchantAge;
    }

    public void setMerchantAge(String merchantAge) {
        this.merchantAge = merchantAge;
    }

    public String getMerchantAddress() {
        return merchantAddress;
    }

    public void setMerchantAddress(String merchantAddress) {
        this.merchantAddress = merchantAddress;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return "Merchant{" +
                "merchantId=" + merchantId +
                ", merchantName='" + merchantName + '\'' +
                ", merchantAge='" + merchantAge + '\'' +
                ", merchantAddress='" + merchantAddress + '\'' +
                ", createDate=" + createDate +
                ", money=" + money +
                ", score=" + score +
                '}';
    }
}
