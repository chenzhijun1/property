package test;

/**
 * title:
 * description:
 * Company: 易宝天创(dataYP.com)
 * Package: test
 * ClassName: Cat
 * Author zhijun.chen
 * CreateTime: 2017/1/16 11:17
 * version 1.0.0
 */
public class Cat extends Animal {

    private String name;

    @Override
    public void say() {
        System.out.println("this is cat say");
    }
}
