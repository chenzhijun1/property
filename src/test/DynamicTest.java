package test;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * title:
 * description:
 * Company: 易宝天创(dataYP.com)
 * Package: test
 * ClassName: DynamicTest
 * Author zhijun.chen
 * CreateTime: 2017/1/16 11:34
 * version 1.0.0
 */
public class DynamicTest {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Animal animal = new Cat();

        Class aclazz = animal.getClass();

        Method[] methods = aclazz.getMethods();
        System.out.print("method name : ");
        for (Method method : methods) {
            System.out.print(method.getName() + ", ");
        }
        System.out.println();
        System.out.println("field name : ");
        Field[] fields = animal.getClass().getFields();
        for (Field field : fields) {
            field.setAccessible(true);
            System.out.print(field.getName() + ", ");
        }


        System.out.println("================================");

        System.out.print("declared method name : ");
        methods = animal.getClass().getDeclaredMethods();
        for (Method method : methods) {
            System.out.print(method.getName() + ",");
        }
        System.out.println();
        System.out.print("declared field name:");
        fields = animal.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            System.out.print(field.getName() + ", ");
        }

        System.out.println("===================");
        Method say = aclazz.getDeclaredMethod("say");
        say.invoke(aclazz.newInstance());

    }
}
