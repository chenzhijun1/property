package test;

/**
 * title:
 * description:
 * Company: 易宝天创(dataYP.com)
 * Package: test
 * ClassName: Animal
 * Author zhijun.chen
 * CreateTime: 2017/1/16 11:17
 * version 1.0.0
 */
public class Animal {

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void say(){
        System.out.println("this is animal say");
    }

}
