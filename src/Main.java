import entity.*;
import utils.PropertiesUtils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * title:
 * description:
 * Company: 易宝天创(dataYP.com)
 * Package: PACKAGE_NAME
 * ClassName: Main
 * Author zhijun.chen
 * CreateTime: 2017/1/16 09:26
 * version 1.0.0
 */
public class Main {
    private static final Map<String, Class<?>> primitiveClazz; // 基本类型的class

    private static final String INT = "int";
    private static final String BYTE = "byte";
//    private static final String CHAR = "char";
    private static final String SHORT = "short";
    private static final String LONG = "long";
    private static final String FLOAT = "float";
    private static final String DOUBLE = "double";
    private static final String BOOLEAN = "boolean";

    static
    {
        primitiveClazz = new HashMap<String, Class<?>>();
        primitiveClazz.put(INT, Integer.class);
        primitiveClazz.put(BYTE, Byte.class);
//        primitiveClazz.put(CHAR, Character.class); // 暂时不支持
        primitiveClazz.put(SHORT, Short.class);
        primitiveClazz.put(LONG, Long.class);
        primitiveClazz.put(FLOAT, Float.class);
        primitiveClazz.put(DOUBLE, Double.class);
        primitiveClazz.put(BOOLEAN, Boolean.class);
    }


    public static void main(String[] args) throws Exception {
//        OuterOSEntity outerOSEntity = new OuterOSEntity(1, "out nick name", "out create date", "out modify date");
//        InnerOSEntity innerOSEntity = new InnerOSEntity();
////        transferData(innerOSEntity,outerOSEntity);
//        transferData2(innerOSEntity,outerOSEntity);
//        System.out.println(innerOSEntity);

//        Merchant merchant = new Merchant();
//        MerchantOut merchantOut = new MerchantOut(112,"out name","12","out address","20120201121212","0.34");
//        merchantOut.setScore(""+16);
//        transferDataByField(merchant,merchantOut);
//        System.out.println(merchant);

        OuterNewObject out = new OuterNewObject("11","5","12","c","999","12.0","99.99","true","aString","20160116172313");
        InnerNewObject in = new InnerNewObject();

        transferDataByField(in,out);
        System.out.println(in);

    }

    public static void transferData(Object inEntity,Object outEntity) throws Exception {
        String value = PropertiesUtils.getValueByKey(inEntity.getClass().getSimpleName()+"|"+outEntity.getClass().getSimpleName());

        Class inClass = inEntity.getClass();
        Class outClass = outEntity.getClass();

        String[] fields = value.split(",");

        for (String str : fields) {
            String[] field = str.split("\\|");

            StringBuffer outSb = new StringBuffer(field[1]);
            outSb.setCharAt(0, Character.toUpperCase(outSb.charAt(0)));
            Method outMethod = outClass.getDeclaredMethod("get" + outSb.toString());

            StringBuffer inSb = new StringBuffer(field[0]);
            inSb.setCharAt(0, Character.toUpperCase(inSb.charAt(0)));
            Method innerMethod = inClass.getDeclaredMethod("set" + inSb.toString(), outMethod.getReturnType());

            innerMethod.invoke(inEntity, outMethod.invoke(outEntity));
        }

    }

    public static void transferDataByField(Object inEntity,Object outEntity) throws Exception {
        String value = PropertiesUtils.getValueByKey(inEntity.getClass().getSimpleName()+"|"+outEntity.getClass().getSimpleName());

        Class inClass = inEntity.getClass();
        Class outClass = outEntity.getClass();

        String[] fields = value.split(",");

        for (String str : fields) {
            String[] field = str.split("\\|");

            StringBuffer outSb = new StringBuffer(field[1]);
            Field outClassDeclaredField = outClass.getDeclaredField(outSb.toString());
            outClassDeclaredField.setAccessible(true);
            Class<?> outType = outClassDeclaredField.getType();
            Object outValue = outClassDeclaredField.get(outEntity);

            StringBuffer inSb = new StringBuffer(field[0]);
            Field inClassDeclaredField = inClass.getDeclaredField(inSb.toString());
            inClassDeclaredField.setAccessible(true);
            Class<?> inType = inClassDeclaredField.getType();

            if(inType == outType){
                inClassDeclaredField.set(inEntity,outValue);
            }else if(inType == Date.class){
                Date date = parseDate(outValue);
                inClassDeclaredField.set(inEntity,date);
            }else{ //if(inType.isPrimitive()){
                String typeName = inType.getSimpleName();
                if(primitiveClazz.containsKey(typeName)){
                    Class<?> primary = primitiveClazz.get(typeName);
                    Constructor<?> constructor = primary.getConstructor(outType);
                    outValue = constructor.newInstance(outValue);
                }else{
                    Constructor<?> constructor = inType.getConstructor(outType);
                    outValue = constructor.newInstance(outValue);
                }
                inClassDeclaredField.set(inEntity,outValue);
            }

        }

    }

    private static Date parseDate(Object outValue) throws ParseException {
        if(outValue instanceof String){
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            Date date = simpleDateFormat.parse(outValue.toString());
            return date;
        }
        return null;
    }

    public void test(){
//        String key = "InnerOSEntity|OuterOSEntity";
//        String value = PropertiesUtils.getValueByKey("InnerOSEntity|OuterOSEntity");
//
//        System.out.println(key + " : " + value);
//
//        Class inClass = InnerOSEntity.class;
//        Class outClass = OuterOSEntity.class;
//
//        Object inObj = inClass.newInstance();
//        OuterOSEntity outObj = new OuterOSEntity(1, "out nick name", "out create date", "out modify date");
//        System.out.println("outObj: " + outObj.toString());
//
//
//        String[] fields = value.split(",");
//
//        for (String str : fields) {
//
//            System.out.println(str.split("\\|")[0] + ", " + str.split("\\|")[1]);
//            String[] field = str.split("\\|");
//
//            StringBuffer outSb = new StringBuffer(field[1]);
//            outSb.setCharAt(0, Character.toUpperCase(outSb.charAt(0)));
//            System.out.println("outSb:" + outSb.toString());
//            Method outMethod = outClass.getDeclaredMethod("get" + outSb.toString());
//
//            StringBuffer inSb = new StringBuffer(field[0]);
//            inSb.setCharAt(0, Character.toUpperCase(inSb.charAt(0)));
//            System.out.println("inSb:" + inSb.toString());
//            Method innerMethod = inClass.getDeclaredMethod("set" + inSb.toString(), outMethod.getReturnType());
//
//            innerMethod.invoke(inObj, outMethod.invoke(outObj));
//        }
//
//        System.out.println("inObj: " + inObj);
    }
}
