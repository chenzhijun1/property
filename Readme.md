
### 介绍

    property 是一个通过反射的方式将对外的系统类值赋值给内部。比如我们可能需要调用微信的 api 接口，而微信的接口
    已经定好了你需要传的变量名，在很多时候，我们系统内部都会根据这些变量建立一个 DTO，而我们实际系统数据库中定义的
    entity 可能和这些 DTO 的名称不一样。很多时候我们都会去调用 get/set 方法来复制，如果少量的还好，如果大量的。
    容易让人不爽。所以本项目通过反射+properties文件配置的方式，希望减轻痛苦。

--------------

### 使用说明

    setting.properties 文件是内部类和外部类的配置，定义了属性名的对应关系，由开发者自己维护；
    Main.java 是主要实例方法；
    MethodTest.java,Test.java 两个文件是Java 使用反射时候的一些实例。
    
 
### 重要说明

    该项目只是从 String 到 Java 基本类型的转换，暂不支持 char 类型，char 包装类中并无从 String->Character
    的构造方法，为啥？因为 String 源码实现里面也是 char[] 数组形式，Java 中对于字符来说，char 是最小的基本单元
    从 String->char 这是不可能发生的。不过如果你想从 String 中获取 char ,可以调用 String 中的 charAt() 方
    法。获取的方式很多，详细的方式可以参考 API 文档。 